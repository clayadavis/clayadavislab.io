.. title: About
.. slug: index
.. tags:
.. category:
.. link:
.. description:
.. type: text


I'm a full-stack data scientist building
data-driven applications with Python, database, and web technologies.
I enjoy working at the boundaries between research and the real world such as
supply chains and sustainability.
You may know me from some of my `projects <projects.html>`_
or perhaps the book I co-authored:

`A First Course in Network Science
<https://github.com/CambridgeUniversityPress/FirstCourseNetworkScience>`_,
published by Cambridge University Press, is available from
`Amazon <https://www.amazon.com/First-Course-Network-Science/dp/1108471137>`_
and other fine booksellers.


Work
====

S&P Global Ratings
    Since Feb 2019: Senior Data Scientist. New York, NY.

DataPad
    Summer 2014: Full-stack development intern. San Francisco, CA.

Anaconda Inc. (formerly Continuum Analytics)
    Summer 2013: Development intern. Austin, TX.


Education
=========

PhD Informatics
    2019 — Indiana University, Bloomington, IN

MA Mathematics
    2011 — Indiana University, Bloomington, IN

BS Mathematics
    2009 — Truman State University, Kirksville, MO


Places
======

Current
    .. class:: list-unstyled

    * St. Louis, MO

Previous
    .. class:: list-unstyled

    * Brooklyn, NY
    * Indianapolis, IN
    * San Francisco, CA
    * Austin, TX
    * Bloomington, IN
    * Kirksville, MO
    * St. Louis, MO
