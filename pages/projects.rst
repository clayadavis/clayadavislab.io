.. title: Projects
.. slug: projects
.. link:
.. description:
.. type: text

Academic
========

`Botometer® <//botometer.iuni.iu.edu>`_ (formerly BotOrNot)
    Scores a Twitter account based on how likely the account is to be a bot.

`Observatory on Social Media (OSoMe) <//osome.iu.edu>`_
    Explore how people spread ideas through online social networks.

`Kinsey Reporter <//kinseyreporter.org>`_
    Global mobile survey platform to share and explore anonymous data about sex.


Hobby
=====

`Card Codex <//cardcodex.com>`_
    Card similarity search for Magic: the Gathering using topic modeling.

`The Free Bestiary <https://clayadavis.gitlab.io/osr-bestiary/>`_
    Free online bestiary for old-school, OSR, and old-school-inspired RPGs.

`dice.party <//dice.party>`_
    Realtime multiplayer dice rooms for online roleplaying games.
