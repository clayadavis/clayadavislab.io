.. title: Connect
.. slug: connect
.. link:
.. description:
.. type: text

Email
    ``cad@clayadavis.net``

GitLab
    `@clayadavis <https://gitlab.com/clayadavis/>`__

GitHub
    `@clayadavis <https://github.com/clayadavis/>`__

LinkedIn
    `/in/clayadavis <http://www.linkedin.com/in/clayadavis>`_
