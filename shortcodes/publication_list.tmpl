{% macro publication(paper) %}
    {% if paper.type %}
        {{ paper.type | title }}:
    {% endif %}

    {% if paper.link %}
    <a class="paper-title" href="{{paper.link}}">
    {% else %}
    <span class="paper-title">
    {% endif %}
        {{ paper.title }}
    {% if paper.link %}</a>{% else %}</span>{% endif %}

    with

    {% for author in paper.authors %}
        {{ author }}
        {%- if loop.index < paper.authors|length - 1 -%}
        ,
        {% elif loop.index == paper.authors|length - 1 %}
        &amp;
        {% endif %}
    {% endfor %}.

    {% if paper.editors %}
    In
    {% endif %}

    {% if paper.journal or paper.collection %}
    <em class="paper-journal-name">{{ paper.journal or paper.collection }}</em>
        {%- if paper.volume -%}, {{ paper.volume }} {%- endif -%}
        {%- if paper.pp -%}: {{ paper.pp }} {%- endif -%}
        ,
    {% endif %}

    {% if paper.venue %}
        {{ paper.venue }},
    {% endif %}

    {% if paper.status %}
        {{ paper.status }}
    {% else %}
        {{ paper.year }}.
    {% endif %}

    {% if paper.award %}
    <strong class="paper-award">{{ paper.award }}.
    {% endif %}

{% endmacro %}

<div class="publications">
    {% for paper in global_data['publications']['papers'] %}
        {% if loop.first or paper.year != loop.previtem.year %}
            {% if not loop.first %}</ul>{% endif %}
            <h2 class="publication-year">{{paper.year}}</h2>
            <ul class="publication-list">
        {% endif %}
        <li class="publication">{{ publication(paper) }}</li>
    {% endfor %}
    </ul>
</div>